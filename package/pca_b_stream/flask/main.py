"""
Copyright CNRS/Inria/UniCA
Contributor(s): Eric Debreuve (eric.debreuve@cnrs.fr) since 2021
SEE COPYRIGHT NOTICE BELOW
"""

from flask import Flask as flask_app_t
from pca_b_stream.flask.html.constants import ABOUT, MAX_IMAGE_SIZE, NAME_MEANING
from pca_b_stream.flask.html.session import SessionInputsAsHTML, SessionOutputsAsHTML
from pca_b_stream.flask.session.constants import APP_NAME, PROJECT_NAME
from pca_b_stream.flask.session.form import form_t
from pca_b_stream.flask.session.processing import ProcessSession
from pca_b_stream.flask.session.session import session_t
from si_fi_o.app import ConfigureApp

HTML_FOLDER = "html"
HOME_PAGE_DETAILS = {
    "html_template": "main.html",
    "name": PROJECT_NAME,
    "name_meaning": NAME_MEANING,
    "about": ABOUT,
    "SessionInputsAsHTML": SessionInputsAsHTML,
    "max_file_size": MAX_IMAGE_SIZE,
    "SessionOutputsAsHTML": SessionOutputsAsHTML,
}


app = flask_app_t(__name__, template_folder=HTML_FOLDER)
ConfigureApp(
    app, HOME_PAGE_DETAILS, form_t, session_t, MAX_IMAGE_SIZE, ProcessSession, APP_NAME
)


if __name__ == "__main__":
    #
    print(
        "####\n"
        "#### Open the http address that appears below "
        "(usually http://127.0.0.1:5000)\n"
        "#### in a web browser to start using PCA-B-Stream\n"
        "####"
    )
    app.run()

"""
COPYRIGHT NOTICE

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

SEE LICENCE NOTICE: file README-LICENCE-utf8.txt at project source root.

This software is being developed by Eric Debreuve, a CNRS employee and
member of team Morpheme.
Team Morpheme is a joint team between Inria, CNRS, and UniCA.
It is hosted by the Centre Inria d'Université Côte d'Azur, Laboratory
I3S, and Laboratory iBV.

CNRS: https://www.cnrs.fr/index.php/en
Inria: https://www.inria.fr/en/
UniCA: https://univ-cotedazur.eu/
Centre Inria d'Université Côte d'Azur: https://www.inria.fr/en/centre/sophia/
I3S: https://www.i3s.unice.fr/en/
iBV: http://ibv.unice.fr/
Team Morpheme: https://team.inria.fr/morpheme/
"""
